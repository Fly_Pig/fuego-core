tarball=iperf-2.0.5.tar.gz

function test_build {
    ./configure --host=$HOST --build=`./config.guess`
    make config.h
    sed -i -e "s/#define HAVE_MALLOC 0/#define HAVE_MALLOC 1/g" -e "s/#define malloc rpl_malloc/\/\* #undef malloc \*\//g" config.h
    sed -i -e '/HEADERS\(\)/ a\#include "gnu_getopt.h"' src/Settings.cpp
    make && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put src/iperf  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	cmd "killall -SIGKILL iperf 2>/dev/null; exit 0"

	# Start iperf server on Jenkins host
	iperf_exec=`which iperf`

	if [ -z $iperf_exec ];
	then 
	 echo "ERROR: Cannot find iperf"
	 false
	else
	 $iperf_exec -s &
	fi

	assert_define BENCHMARK_IPERF_SRV

	if [ "$BENCHMARK_IPERF_SRV" = "default" ]; then
	  srv=$SRV_IP
	else
	  srv=$BENCHMARK_IPERF_SRV
	fi

	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./iperf -c $srv -t 15; ./iperf -c $srv -d -t 15" $FUEGO_HOME/fuego.$TESTDIR/${TESTDIR}.log
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
