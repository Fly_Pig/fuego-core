tarball=pi_tests.tar.bz2

function test_build {
    sed -i -e "1d" ./Makefile
    make CC="$CC" LD="$LD" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put ./pi_stress  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	assert_define FUNCTIONAL_PI_TESTS_INVERSIONS
	assert_define FUNCTIONAL_PI_TESTS_GROUPS
	assert_define FUNCTIONAL_PI_TESTS_TIMEOUT
 
	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./pi_stress --groups=$FUNCTIONAL_PI_TESTS_GROUPS --inversions=$FUNCTIONAL_PI_TESTS_INVERSIONS & sleep $FUNCTIONAL_PI_TESTS_TIMEOUT; killall -9 pi_stress; true"  
}

. $FUEGO_SCRIPTS_PATH/stress.sh
