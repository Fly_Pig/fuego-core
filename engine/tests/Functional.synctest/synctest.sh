tarball=synctest.tar.gz

function test_build {
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put synctest  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
    assert_define FUNCTIONAL_SYNCTEST_MOUNT_BLOCKDEV
    assert_define FUNCTIONAL_SYNCTEST_MOUNT_POINT
    assert_define FUNCTIONAL_SYNCTEST_LEN
    assert_define FUNCTIONAL_SYNCTEST_LOOP
    
    hd_test_mount_prepare $FUNCTIONAL_SYNCTEST_MOUNT_BLOCKDEV $FUNCTIONAL_SYNCTEST_MOUNT_POINT
    report "cd $FUNCTIONAL_SYNCTEST_MOUNT_POINT/fuego.$TESTDIR; $FUEGO_HOME/fuego.$TESTDIR/synctest $FUNCTIONAL_SYNCTEST_LEN $FUNCTIONAL_SYNCTEST_LOOP"

    hd_test_clean_umount $FUNCTIONAL_SYNCTEST_MOUNT_BLOCKDEV $FUNCTIONAL_SYNCTEST_MOUNT_POINT
}

function test_processing {
	log_compare "$TESTDIR" "1" "PASS : sync interrupted" "p"
}

. $FUEGO_SCRIPTS_PATH/functional.sh

