
tarball=dung-3.4.25-m2.tar.gz

function test_build  {
    patch -N -s -p1 < $TEST_HOME/switch-bash-to-sh-arch_timer-interrupt-lager.patch
    touch test_suit_ready
}

function test_deploy {
    put -r ./* $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
    report "cd $FUEGO_HOME/fuego.$TESTDIR/arch_timer; ./arch_timer-interrupt-lager.sh ; ./dmesg-lager.sh;  ./proc-interrupts-lager.sh"
}

function test_processing {
    assert_define FUNCTIONAL_ARCH_TIMER_RES_LINES_COUNT

    check_capability "RENESAS"

    log_compare "$TESTDIR" $FUNCTIONAL_ARCH_TIMER_RES_LINES_COUNT "Test passed" "p"
}

. $FUEGO_SCRIPTS_PATH/functional.sh

