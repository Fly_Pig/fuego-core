tarball=dung-3.4.25-m2.tar.gz

function test_build  {
    touch test_suite_ready
}

function test_deploy {
    put -r ./* $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
    report "cd $FUEGO_HOME/fuego.$TESTDIR/cmt; ./cmt-interrupt.sh; ./dmesg.sh; ./proc-interrupts.sh"
}

function test_processing {
    check_capability "RENESAS"

    log_compare "$TESTDIR" $FUNCTIONAL_CMT_LINES_COUNT "Test passed" "p"
}

. $FUEGO_SCRIPTS_PATH/functional.sh

