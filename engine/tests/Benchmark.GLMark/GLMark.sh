tarball=GLMark-0.5.2.1.tar.gz

function test_build {
#       patch -p1 -N -s < ../../tarballs/$TESTDIR.patch
    export PATH=/usr/local/bin:$PATH;
    CFLAGS+="${CFLAGS}" $CXX  -O0 -ggdb -I${SDKROOT}/usr/include \
            -L${SDKROOT}/usr/lib -Wl,-rpath-link=${SDKROOT}/usr/lib \
            -L${SDKROOT}/lib \
             *.cpp -o glmark -lSDL -lGL \
            -lGLU -lGLEW && touch test_suite_ready || build_error "error while building test"
#               -Wl,--allow-shlib-undefined *.cpp -o glmark -lSDL -lGL \
}

function test_deploy {
	put -r glmark data  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	safe_cmd "{ cd $FUEGO_HOME/fuego.$TESTDIR; export DISPLAY=:0; xrandr |awk '/\*/ {split(\$1, a, \"x\"); print a[1], a[2], 32, 1}' > params; ./glmark &>   < params; } || { [ \$? -eq 142 ] && exit 0; }"
}

. $FUEGO_SCRIPTS_PATH/benchmark.sh
