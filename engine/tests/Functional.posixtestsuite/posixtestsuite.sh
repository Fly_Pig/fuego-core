tarball=posixtestsuite-1.5.2.tar.gz

function test_build {
    patch -p1 -N -s < $TEST_HOME/posixtest-posix-linux.patch
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put -r ./*  $FUEGO_HOME/fuego.$TESTDIR/
}

function test_run {
	report "cd $FUEGO_HOME/fuego.$TESTDIR; ./execute.sh"  
}

function test_processing {
	PASSED="PASSED:  922"
	FAILED="FAILED:  69"
	UNRESOLVED="UNRESOLVED:  7"
	UNSUPPORTED="UNSUPPORTED:  93"
	UNTESTED="UNTESTED:  66"

	log_compare "$TESTDIR" "1" "${PASSED}" "p"
	log_compare "$TESTDIR" "1" "${FAILED}" "f"
	log_compare "$TESTDIR" "1" "${UNRESOLVED}" "unr"
	log_compare "$TESTDIR" "1" "${UNSUPPORTED}" "uns"
	log_compare "$TESTDIR" "1" "${UNTESTED}" "unt"
}

. $FUEGO_SCRIPTS_PATH/functional.sh
